import React, { Component } from 'react';
import PropTypes from 'prop-types';

class DetailsItem extends Component {
    edit = details => {
      const { props: { setDetailsForEdit } } = this;

      setDetailsForEdit(details);
    }

    delete = details => {
      const { props: { deleteDetails } } = this;

      deleteDetails(details);
    }

    render() {
      const { props: { details } } = this;

      return (
        <li className="create-details">
          <span>{ details.student }</span>
          <span>{ details.mark }</span>
          <span>{ details.date }</span>
          <button type="button" onClick={() => this.edit(details)}>Edit</button>
          <button type="button" onClick={() => this.delete(details)}>Delete</button>
        </li>
      );
    }
}
DetailsItem.propTypes = {
  deleteDetails: PropTypes.func.isRequired,
  details: PropTypes.shape({
    date: PropTypes.string,
    mark: PropTypes.string,
    rating: PropTypes.number,
    student: PropTypes.string,
  }).isRequired,
  setDetailsForEdit: PropTypes.func.isRequired,
};
export default DetailsItem;
