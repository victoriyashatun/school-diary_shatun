import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NewDetailsPopup extends Component {
  state = {
    date: '',
    mark: '',
    student: '',
  };

  componentDidMount() {
    const { props: { detailsForEdit, editFlag } } = this;

    if (editFlag) {
      this.setState({
        date: detailsForEdit.date,
        mark: detailsForEdit.mark,
        student: detailsForEdit.student,
      });
    }
  }

  changeStudent = val => {
    const { target: { value } } = val;

    this.setState({ student: value });
  };

  changeMark = val => {
    const { target: { value } } = val;

    this.setState({ mark: value });
  };

  changeDate = val => {
    const { target: { value } } = val;

    this.setState({ date: value });
  };

  close = () => {
    const { props } = this;

    props.toggleNewDetailsPopup();
  };

  add = () => {
    const { state: newStudent } = this;

    newStudent.id = new Date().getTime();
    const subjectArray = JSON.parse(localStorage.subjectArray);

    if (newStudent.mark > 0 && newStudent.mark < 6 && newStudent.student && newStudent.date) {
      subjectArray.forEach(item => {
        const { props: subjectId } = this;

        if (Number(item.id) === Number(subjectId.subjectId)) {
          item.students.push(newStudent);
        }
      });
      subjectArray.forEach(item => {
        const { props: subjectId } = this;

        if (Number(item.id) === Number(subjectId.subjectId)) {
          let rating = 0;

          item.students.forEach(student => {
            rating += Number(student.mark);
          });
          item.rating = Math.round(rating / item.students.length);
        }
      });

      localStorage.setItem('subjectArray', JSON.stringify(subjectArray));
      this.close();
    }
  };

  update = () => {
    const subjectArray = JSON.parse(localStorage.subjectArray);
    const { state: { mark, student: name, date } } = this;

    if (mark > 0 && mark < 6 && name && date) {
      subjectArray.forEach(item => {
        const { props: subjectId } = this;

        if (Number(item.id) === Number(subjectId.subjectId)) {
          item.students.forEach(student => {
            if (student.id === this.props.detailsForEdit.id) {
              student.student = name;
              student.mark = mark;
              student.date = date;
            }
          });
        }
      });

      subjectArray.forEach(item => {
        console.log(item)
        const { props: { subjectId } } = this;

        if (Number(item.id) === Number(subjectId)) {
          let rating = 0;

          item.students.forEach(student => {
            rating += Number(student.mark);
          });
          item.rating = Math.round(rating / item.students.length);
        }
      });
      console.log('subjectArray', subjectArray)
      localStorage.setItem('subjectArray', JSON.stringify(subjectArray));
      this.close();
    }
  };

  render() {
    const { state: { student, mark, date } } = this;
    const { props: { editFlag } } = this;

    return (
      <div className="new-details-popup">
        <div>
          <button type="button" onClick={this.close}>Close</button>
        </div>
        <div className="details-popup">
          <input
            type="text"
            placeholder="Student"
            value={student}
            onChange={event => this.changeStudent(event)}
          />
          <input
            type="number"
            placeholder="Mark"
            value={mark}
            onChange={event => this.changeMark(event)}
          />
          <input
            type="date"
            placeholder="Date"
            value={date}
            onChange={event => this.changeDate(event)}
          />
          {editFlag ? (
            <button type="button" onClick={this.update}>Update</button>
          ) : (
            <button type="button" onClick={this.add}>Add</button>
          )}
        </div>
      </div>
    );
  }
}
NewDetailsPopup.propTypes = {
  editFlag: PropTypes.bool.isRequired,
  toggleNewDetailsPopup: PropTypes.func.isRequired,
};
export default NewDetailsPopup;
