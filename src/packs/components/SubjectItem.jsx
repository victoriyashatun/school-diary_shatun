import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class SubjectItem extends Component {

  edit = subject => {
    const { props: { setSubjectForEdit } } = this;
    setSubjectForEdit(subject);
  }

  delete = subject => {
    const { props: { deleteSubject } } = this;

    deleteSubject(subject);
  }

  render() {
    const { props: { subject } } = this;

    return (
      <li>
        <Link to={`/details?${subject.id}`}>
          <span>
            { subject.subject }
          </span>
        </Link>
        <span className="teacher">{ subject.teacher }</span>
        <span className="rating">
            Rating:
          { subject.rating ? subject.rating : 0 }
        </span>
        <button className="edit" type="button" onClick={() => this.edit(subject)}>Edit</button>
        <button type="button" onClick={() => this.delete(subject)}>Delete</button>
      </li>
    );
  }

}

SubjectItem.propTypes = {
  deleteSubject: PropTypes.func.isRequired,
  setSubjectForEdit: PropTypes.func.isRequired,
  subject: PropTypes.shape({
    id: PropTypes.number,
    subject: PropTypes.string,
    teacher: PropTypes.string,
    rating: PropTypes.number,
  }).isRequired,
};
export default SubjectItem;
