import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NewSubjectPopup extends Component {
    state = {
      subject: '',
      teacher: '',
    };

    componentDidMount() {
      const { props: { subjectForEdit, editFlag } } = this;

      if (editFlag) {
        this.setState({
          subject: subjectForEdit.subject,
          teacher: subjectForEdit.teacher,
        });
      }
    }

  changeSubject = val => {
    const { target: { value } } = val;

    this.setState({ subject: value });
  };

  changeTeacher = val => {
    const { target: { value } } = val;

    this.setState({ teacher: value });
  };

  close = () => {
    const { props } = this;

    props.toggleNewSubjectPopup();
  };

  add = () => {
    const { state: newSubject } = this;

    if (newSubject.subject && newSubject.teacher){
      newSubject.id = new Date().getTime();
      newSubject.students = [];
      newSubject.rating = 0;
      this.props.addSubject(newSubject);
      this.props.toggleNewSubjectPopup();
    }
  };

  update = () => {
    const { state: { subject, teacher } } = this;
    const { props } = this;
    const { subjectForEdit: { id } } = props;
    const updatedSubject = {
      id,
      subject,
      teacher,
    }

    props.updateSubject(updatedSubject);
    this.close();
  }

  render() {
    const { state: { subject, teacher } } = this;
    const { props: { editFlag } } = this;

    return (
      <div className="new-subject-popup">
        <div>
          <button type="button" onClick={this.close}>Close</button>
        </div>
        <div className="create-subject">
          Subject:
          <input
            type="text"
            placeholder="Subject"
            value={subject}
            onChange={event => this.changeSubject(event)}
          />
          Teacher:
          <input
            type="text"
            placeholder="Teacher"
            value={teacher}
            onChange={event => this.changeTeacher(event)}
          />
          {editFlag ? (
            <button type="button" onClick={this.update}>Update</button>
          ) : (
            <button type="button" onClick={this.add}>Add</button>
          )}
        </div>
      </div>
    );
  }
}
NewSubjectPopup.propTypes = {
  addSubject: PropTypes.func.isRequired,
  editFlag: PropTypes.bool.isRequired,
  toggleNewSubjectPopup: PropTypes.func.isRequired,
  updateSubject: PropTypes.func.isRequired,
};
export default NewSubjectPopup;
