import React, { Component } from 'react';
import './Details.css';
import NewDetailsPopup from '../../components/newDetailsPopup';
import DetailsItem from '../../components/detailsItem';
import { Link } from 'react-router-dom';


class Details extends Component {
  state = {
    editFlag: false,
    detailsForEdit: {},
    showNewDetailsPopup: false,
    studentsArray: [],
    subject: {},
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    if (localStorage.subjectArray) {
      const { props: { location: { search: stringId } } } = this;
      const [, subjectId] = stringId.split('?');
      const subjectArray = JSON.parse(localStorage.subjectArray);
      const subject = subjectArray.find(item => Number(item.id) === Number(subjectId));

      subject.students && this.setState({ studentsArray: subject.students, subject });
    }
  };

  toggleNewDetailsPopup = () => {
    const { state: { editFlag, showNewDetailsPopup } } = this;

    if (editFlag) {
      this.setState({ showNewDetailsPopup: false, editFlag: false });
    } else {
      this.setState({ showNewDetailsPopup: !showNewDetailsPopup });
    }
    this.loadData();
  };

  addStudent = newStudent => {
    const { state: { studentsArray } } = this;

    studentsArray.push(newStudent);
    this.setState({ studentsArray });
  };

  setDetailsForEdit = details => {
    this.setState({ DetailsForEdit: details, editFlag: true });
  }

  deleteDetails = details => {
    const subjectArray = JSON.parse(localStorage.subjectArray);
    const { props: { location: { search: stringId } } } = this;
    const [, subjectId] = stringId.split('?');

    subjectArray.forEach(item => {
      if (Number(item.id) === Number(subjectId)) {
        item.students.forEach((student, index) => {
          if (Number(student.id) === Number(details.id)) {
            item.students.splice(index, 1);
          }
        });
      }
    });

    subjectArray.forEach(item => {
      if (item.id === Number(subjectId)) {
        let rating = 0;

        item.students.forEach(student => {
          rating += Number(student.mark);
        });
        item.rating = Math.round(rating / item.students.length);
      }
    });

    localStorage.setItem('subjectArray', JSON.stringify(subjectArray));
    this.loadData();
  }

  updateDetails = () => {
    console.log('updated');
  }

  render() {
    const {
      showNewDetailsPopup,
      studentsArray,
      DetailsForEdit,
      editFlag,
      subject,
    } = this.state;
    const { props: { location: { search: stringId } } } = this;
    const [, subjectId] = stringId.split('?');

    return (
      <div>
        <Link className="home" to="/">
          Home
        </Link>
        <hr />
        <button type="button" className="add-details" onClick={this.toggleNewDetailsPopup}>
          Add Details
        </button>
        <div>
           Rating:
          {subject && subject.rating}
        </div>
        {(showNewDetailsPopup || editFlag) && (
          <NewDetailsPopup
            toggleNewDetailsPopup={this.toggleNewDetailsPopup}
            addStudent={this.addStudent}
            detailsForEdit={DetailsForEdit}
            editFlag={editFlag}
            updateDetails={this.updateDetails}
            subjectId={subjectId}
          />
        )}
        <div>
          <ul className="add-details-form">
            {studentsArray.map((item, index) => (
              <DetailsItem
                details={item}
                setDetailsForEdit={this.setDetailsForEdit}
                deleteDetails={this.deleteDetails}
                key={index}
              />
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Details;
