import React, { Component } from 'react';
import './Subject.css';
import NewSubjectPopup from '../../components/NewSubjectPopup';
import SubjectItem from '../../components/SubjectItem';

class Subject extends Component {
  state = {
    editFlag: false,
    rating: null,
    showNewSubjectPopup: false,
    subjectArray: [],
    subjectForEdit: {},
    sortRating: 'asc',
    sortWay: 'down',
  };

  componentDidMount() {
    if (localStorage.subjectArray) {
      this.setState({ subjectArray: JSON.parse(localStorage.subjectArray) });
    }
  }

  toggleNewSubjectPopup = () => {
    const { state: { editFlag, showNewSubjectPopup } } = this;

    if (editFlag) {
      this.setState({ showNewSubjectPopup: false, editFlag: false });
    } else {
      this.setState({ showNewSubjectPopup: !showNewSubjectPopup });
    }
  };

  addSubject = newSubject => {
    const { state: { subjectArray } } = this;

    subjectArray.push(newSubject);
    localStorage.setItem('subjectArray', JSON.stringify(subjectArray));
    this.setState({ subjectArray });
  };

  setSubjectForEdit = subject => {
    this.setState({ subjectForEdit: subject, editFlag: true });
  };

  updateSubject = updatedSubject => {
    const { state: { subjectArray, subjectForEdit } } = this;

    subjectArray.forEach((item, index) => {
      if (item.id === subjectForEdit.id) {
        subjectArray.splice(index, 1, { ...subjectForEdit, ...updatedSubject });
      }
    });
    localStorage.setItem('subjectArray', JSON.stringify(subjectArray));
    this.setState({ subjectArray });
  };

  deleteSubject = subject => {
    const { state: { subjectArray } } = this;

    subjectArray.forEach((item, index) => {
      if (item.id === subject.id) {
        subjectArray.splice(index, 1);
      }
    });
    localStorage.setItem('subjectArray', JSON.stringify(subjectArray));

    this.setState({ subjectArray });
  };

  sort = fieldName => {
    const { state: { sortWay, subjectArray } } = this;

    if (sortWay === 'asc') {
      subjectArray.sort((a, b) => {
        if (a[fieldName] > b[fieldName]) {
          return 1;
        }
        if (a[fieldName] < b[fieldName]) {
          return -1;
        }

        return 0;
      });
      this.setState({ subjectArray, sortWay: 'desc' });
    } else {
      subjectArray.sort((a, b) => {
        if (a[fieldName] < b[fieldName]) {
          return 1;
        }
        if (a[fieldName] > b[fieldName]) {
          return -1;
        }

        return 0;
      });
      this.setState({ subjectArray, sortWay: 'asc', activeSort: fieldName });
    }
  };

  render() {
    const {
      state: {
        activeSort,
        showNewSubjectPopup,
        subjectArray,
        subjectForEdit,
        editFlag,
        sortWay,
      },
    } = this;

    return (
      <div>
        {(showNewSubjectPopup || editFlag) && (
          <NewSubjectPopup
            toggleNewSubjectPopup={this.toggleNewSubjectPopup}
            addSubject={this.addSubject}
            subjectForEdit={subjectForEdit}
            editFlag={editFlag}
            updateSubject={this.updateSubject}
          />
        )}
        <button type="button" className="add-subject" onClick={this.toggleNewSubjectPopup}>
          Add Subject
        </button>
        <div>
          <div>
            <button type="button" onClick={() => this.sort('subject')}>
              Sort Title
              {(activeSort === 'subject')
              && ((sortWay === 'asc') ? <span>&uarr;</span> : <span>&darr;</span>)}
            </button>
            <button type="button" onClick={() => this.sort('rating')}>
              Sort Rating
              {(activeSort === 'rating')
            && ((sortWay === 'asc') ? <span>&uarr;</span> : <span>&darr;</span>)}
            </button>
          </div>
          <ul className="list">
            {subjectArray.map((subject, index) => (
              <SubjectItem
                subject={subject}
                setSubjectForEdit={this.setSubjectForEdit}
                deleteSubject={this.deleteSubject}
                key={index}
              />
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Subject;
