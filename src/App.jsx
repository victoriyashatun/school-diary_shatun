import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import Subject from './packs/routes/subjects/Subject';
import Details from './packs/routes/details/Details';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Subject} />
        <Route path="/details" component={Details} />
      </Switch>
    </Router>
  );
}

export default App;
